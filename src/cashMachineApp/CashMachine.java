package cashMachineApp;

public abstract class CashMachine {

    protected CashMachine succesor;

    public abstract double getBanknote();

    public abstract void setBanknote(double banknote);

    public abstract int getNumberOfnominal();

    public abstract void setNumberOfnominal(int numberOfnominal);

    public CashMachine getSuccesor() {
        return succesor;
    }

    public void setSuccesor(CashMachine succesor) {
        this.succesor = succesor;
    }

    public void payMoney(PaymentOrder paymentOrder) {
        int nominalNumber = 0;
        double sum = getNumberOfnominal();
        for (double i = getBanknote(); i <= (getBanknote() * getNumberOfnominal()); i += getBanknote()) {
            if (paymentOrder.getAmount() - getBanknote() >= 0) {
                paymentOrder.setAmount(paymentOrder.getAmount() - getBanknote());
                nominalNumber++;
            }
        }

        setNumberOfnominal(getNumberOfnominal() - nominalNumber);

        System.out.println("Wydano: " + getBanknote() + " w ilości: " + nominalNumber);

        if (paymentOrder.getAmount() > 0) {
            if (succesor == null) {
                System.out.println("Nie ma dostępnych nominałów");
            } else {
                succesor.payMoney(paymentOrder);
            }
        }

    }
}
