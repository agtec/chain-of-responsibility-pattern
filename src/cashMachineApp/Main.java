package cashMachineApp;

public class Main {

    public static void main(String[] args) {

        CashMachine nominal20 = new Nominal20();
        CashMachine nominal50 = new Nominal50();
        CashMachine nominal100 = new Nominal100();
        CashMachine nominal200 = new Nominal200();

        PaymentOrder paymentOrder = new PaymentOrder(750);

        nominal200.setSuccesor(nominal100);
        nominal100.setSuccesor(nominal50);
        nominal50.setSuccesor(nominal20);

        nominal200.payMoney(paymentOrder);
    }
}
