package cashMachineApp;

public class Nominal20 extends CashMachine {
    protected double banknote = 20;
    protected int numberOfnominal = 10;

    public double getBanknote() {
        return banknote;
    }

    public void setBanknote(double banknote) {
        this.banknote = banknote;
    }

    @Override
    public int getNumberOfnominal() {
        return numberOfnominal;
    }

    @Override
    public void setNumberOfnominal(int numberOfnominal) {
        this.numberOfnominal = numberOfnominal;
    }

}
