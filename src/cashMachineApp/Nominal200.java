package cashMachineApp;

public class Nominal200 extends CashMachine {

    protected double banknote = 200;
    protected int numberOfnominal = 2;

    public double getBanknote() {
        return banknote;
    }

    public void setBanknote(double banknote) {
        this.banknote = banknote;
    }

    @Override
    public int getNumberOfnominal() {
        return numberOfnominal;
    }

    @Override
    public void setNumberOfnominal(int numberOfnominal) {
        this.numberOfnominal = numberOfnominal;
    }
}
