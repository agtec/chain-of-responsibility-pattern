package cashMachineApp;

public class Nominal50 extends CashMachine {

    protected double banknote = 50;
    protected int numberOfnominal = 3;

    public double getBanknote() {
        return banknote;
    }

    public void setBanknote(double banknote) {
        this.banknote = banknote;
    }

    @Override
    public int getNumberOfnominal() {
        return numberOfnominal;
    }

    @Override
    public void setNumberOfnominal(int numberOfnominal) {
        this.numberOfnominal = numberOfnominal;
    }
}
