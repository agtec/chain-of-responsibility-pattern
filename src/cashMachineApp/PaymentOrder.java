package cashMachineApp;

public class PaymentOrder {

    private double amount;

    public PaymentOrder(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
